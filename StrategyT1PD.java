package play;

import java.util.Iterator;
import java.util.Random;

import gametree.GameNode;
import gametree.GameNodeDoesNotExistException;
import play.exception.InvalidStrategyException;

public class StrategyT1PD  extends Strategy {
	
	private boolean p1OppHasDef = false;
	private boolean p2OppHasDef = false;

	@Override
	public void execute() throws InterruptedException {
		while(!this.isTreeKnown()) {
			System.err.println("Waiting for game tree to become available.");
			Thread.sleep(1000);
		}
		while(true) {
			PlayStrategy myStrategy = this.getStrategyRequest();
			if(myStrategy == null) //Game was terminated by an outside event
				break;	
			boolean playComplete = false;
						
			while(! playComplete ) {
				System.out.println("*******************************************************");
				if(myStrategy.getFinalP1Node() != -1) {
					GameNode finalP1 = this.tree.getNodeByIndex(myStrategy.getFinalP1Node());
					GameNode fatherP1 = null;
					if(finalP1 != null) {
						try {
							fatherP1 = finalP1.getAncestor();
						} catch (GameNodeDoesNotExistException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.print("Last round as P1: " + showLabel(fatherP1.getLabel()) + "|" + showLabel(finalP1.getLabel()));
						System.out.println(" -> (Me) " + finalP1.getPayoffP1() + " : (Opp) "+ finalP1.getPayoffP2());
					}
				}			
				if(myStrategy.getFinalP2Node() != -1) {
					GameNode finalP2 = this.tree.getNodeByIndex(myStrategy.getFinalP2Node());
					GameNode fatherP2 = null;
					if(finalP2 != null) {
						try {
							fatherP2 = finalP2.getAncestor();
						} catch (GameNodeDoesNotExistException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.print("Last round as P2: " + showLabel(fatherP2.getLabel()) + "|" + showLabel(finalP2.getLabel()));
						System.out.println(" -> (Opp) " + finalP2.getPayoffP1() + " : (Me) "+ finalP2.getPayoffP2());
					}
				}
				GameNode rootNode = tree.getRootNode();
				int n1 = rootNode.numberOfChildren();
				int n2=rootNode.getChildren().next().numberOfChildren();
				String[] labelsP1 = new String[n1];
				String[] labelsP2 = new String[n2];
				int[][] U1 = new int[n1][n2];
				int[][] U2 = new int[n1][n2];
				Iterator<GameNode> childrenNodes1 = rootNode.getChildren();
				GameNode childNode1;
				GameNode childNode2;
				int i = 0;
				int j = 0;
				while(childrenNodes1.hasNext()) {
					childNode1 = childrenNodes1.next();
					labelsP1[i] = childNode1.getLabel();	
					j = 0;
					Iterator<GameNode> childrenNodes2 = childNode1.getChildren();
					while(childrenNodes2.hasNext()) {
						childNode2 = childrenNodes2.next();
						if (i==0) labelsP2[j] = childNode2.getLabel();
						U1[i][j] = childNode2.getPayoffP1();
						U2[i][j] = childNode2.getPayoffP2();
						j++;
					}	
					i++;
				}
				//showMoves(1,labelsP1);
				//showMoves(2,labelsP2);
				//showUtility(1,n1,n2,U1);
				//showUtility(2,n1,n2,U2);
				double[] strategyP1 = setStrategyForT1PD(1,labelsP1,myStrategy, U1);
				double[] strategyP2 = setStrategyForT1PD(2,labelsP2,myStrategy, U2);
				//showStrategy(1,strategyP1,labelsP1);
				//showStrategy(2,strategyP2,labelsP2);			
				try{
					this.provideStrategy(myStrategy);
					playComplete = true;
				} catch (InvalidStrategyException e) {
					System.err.println("Invalid strategy: " + e.getMessage());;
					e.printStackTrace(System.err);
				} 
			}
		}
		
	}
	
	public String showLabel(String label) {
		return label.substring(label.lastIndexOf(':')+1);
	}
	
	public void showMoves(int P, String[] labels) {
		System.out.println("Moves Player " + P + ":");
		for (int i = 0; i<labels.length; i++) System.out.println("   " + showLabel(labels[i]));
	}
	
	public void showUtility(int P, int n1, int n2, int[][] M) {
		System.out.println("Utility Player " + P + ":");
		for (int i = 0; i<n1; i++) {
			for (int j = 0; j<n2; j++) System.out.print("| " + M[i][j] + " ");
			System.out.println("|");
		}
	}
	
	public double[] setStrategy(int P, String[] labels, PlayStrategy myStrategy) {
		int n = labels.length;
		double[] strategy = new double[n];
		for (int i = 0; i<n; i++)  strategy[i] = 0;
		if (P==1) {
			strategy[0] = 1;
		}
		else {
			strategy[0] = 0.5;
			strategy[1] = 0.5;
		}
		for (int i = 0; i<n; i++) myStrategy.put(labels[i], strategy[i]);
		return strategy;
	}
	
	/*
	 * Strategy for Tournament 1 - PD
	 */
		public double[] setStrategyForT1PD(int P, String[] labels, PlayStrategy myStrategy, int[][] utils) {
			int n = labels.length;
			double[] strategy = new double[n];
			
			//strategy[0] = Coop, strategy[1] = Defect
			//Grim Trigger - Round 1
			if(myStrategy.probabilityForNextIteration() == 1.0){
				if(P==1){
					//Defect
					if(myStrategy.getMaximumNumberOfIterations() <= 2){
						strategy[0] = 0.0;
						strategy[1] = 1.0;
					} else {
						if(!p1OppHasDef){
							//Check if the opp has defected in the last round
							p1OppHasDef = myStrategy.getLastRoundOpponentScoreAsP1(this.tree) == 4 ? true : false;
						}
						if(p1OppHasDef){
							//Defect
							System.out.println("P1 Opp Has def");
							strategy[0] = 0.0;
							strategy[1] = 1.0;
						} else {
							//Coop
							strategy[0] = 1.0;
							strategy[1] = 0.0;
						}
					}
				} else {
					if(myStrategy.getMaximumNumberOfIterations() <= 2  || myStrategy.probabilityForNextIteration() <= 0.1){
						//Defect
						strategy[0] = 0.0;
						strategy[1] = 1.0;
					} else {
						if(!p2OppHasDef){
							//Check if the opp has defected in the last round
							p2OppHasDef = myStrategy.getLastRoundOpponentScoreAsP2(this.tree) == 4 ? true : false;
						}
						if(p2OppHasDef){
							//Defect
							System.out.println("P2 Opp Has def");
							strategy[0] = 0.0;
							strategy[1] = 1.0;
						} else {
							//Coop
							strategy[0] = 1.0;
							strategy[1] = 0.0;
						}
					}
				}
			} else {
				//Strategy for Round 2 and 3
				/*
				 * Tit-for-Tat
				 * Start by cooperating, and for the next rounds copy the oponent's last move.
				 * In addition, if a generated amplified random number betwen 0 and 1 
				 * is bigger than the probability to continue,
				 * the player will Defect. Also in the last 2 rounds, the player always Defect.
				 */
				double probAmplified = amplifyProb(myStrategy.probabilityForNextIteration());
				Random random = new Random();
				if(P==1){
					//Strategy for P1
					double randomProb = random.nextDouble();
					if(myStrategy.getMaximumNumberOfIterations() <= 2 || randomProb > probAmplified){
						//Always Defect in these cases
						strategy[0] = 0.0;
						strategy[1] = 1.0;
					} else {
						int opponentLastRoundScore = myStrategy.getLastRoundOpponentScoreAsP1(this.tree);
						p1OppHasDef = lastOppMove(opponentLastRoundScore);
						if(!p1OppHasDef){
							//If the opponent cooperated, cooperate as well
							strategy[0] = 1.0;
							strategy[1] = 0.0;
						} else {
							//If the opponent defected, defect as well
							strategy[0] = 0.0;
							strategy[1] = 1.0;
						}
					}
				} else {
					//Strategy for P2
					double randomProb = random.nextDouble();
					if(myStrategy.getMaximumNumberOfIterations() <= 2 || randomProb > probAmplified){
						//Always Defect in these cases
						strategy[0] = 0.0;
						strategy[1] = 1.0;
					} else {
						int opponentLastRoundScore = myStrategy.getLastRoundOpponentScoreAsP2(this.tree);
						p2OppHasDef = lastOppMove(opponentLastRoundScore);
						if(!p2OppHasDef){
							//If the opponent cooperated, cooperate as well
							strategy[0] = 1.0;
							strategy[1] = 0.0;
						} else {
							//If the opponent defected, defect as well
							strategy[0] = 0.0;
							strategy[1] = 1.0;
						}
					}
				}
			}
			System.out.println("It: "+ myStrategy.getMaximumNumberOfIterations());
			System.out.println("Player "+P+", p1 Opp Has def: "+p1OppHasDef+", p2 Opp Has def: "+p2OppHasDef);
			
			for (int i = 0; i<n; i++) myStrategy.put(labels[i], strategy[i]);
			return strategy;
		}
	
	private double amplifyProb(double probabilityForNextIteration) {
			if(probabilityForNextIteration <= 0.5){
				return 2 * Math.pow(probabilityForNextIteration, 2);
			} else {
				return -2 * Math.pow(probabilityForNextIteration - 1, 2) + 1;
			}
		}

	private boolean lastOppMove(int opponentLastRoundScore) {
		//Return true if Defect, false if Coop
		if(opponentLastRoundScore == 4 || opponentLastRoundScore == 1){
			return true;
		} else {
			//If opp score is 3 or 0 or hasn't played yet, which in that case is 0
			return false;
		}
	}

	public void showStrategy(int P, double[] strategy, String[] labels) {
		System.out.println("Strategy Player " + P + ":");
		for (int i = 0; i<labels.length; i++) System.out.println("   " + strategy[i] + ":" + showLabel(labels[i]));
	}

}
